package com.pandaz.twitter

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.pandaz.twitter.ui.intro.IntroActivity
import org.junit.Rule
import org.junit.runner.RunWith
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

/**
 * Created by LEE SU HO on 2019-05-05.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

@RunWith(AndroidJUnit4::class)
abstract class BaseViewModelTest {
    @Rule
    @JvmField
    val activityRule = ActivityTestRule(IntroActivity::class.java)

    protected fun ui(block: () -> Unit) = activityRule.runOnUiThread(block)

    protected fun await(block: () -> Unit) {
        val latch = CountDownLatch(1)
        latch.await(2, TimeUnit.SECONDS)
        block()
    }

    protected fun activity() = activityRule.activity

}