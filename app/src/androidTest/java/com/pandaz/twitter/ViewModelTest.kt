package com.pandaz.twitter

import android.support.test.runner.AndroidJUnit4
import com.pandaz.twitter.network.dto.TweetDto
import com.pandaz.twitter.ui.detail.TweetDetailFragmentViewModel
import com.pandaz.twitter.ui.timeline.TimelineFragmentViewModel
import com.pandaz.twitter.util.PrefHelper
import com.pandaz.twitter.util.viewModel
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by LEE SU HO on 2019-05-05.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

@RunWith(AndroidJUnit4::class)
class ViewModelTest : BaseViewModelTest() {

    @Before
    fun setUp() {
        val token = PrefHelper.getTokenSecret()
        val secret = PrefHelper.getTokenSecret()
        assertEquals(token.isNullOrEmpty(), false)
        assertEquals(secret.isNullOrEmpty(), false)
    }

    @Test
    fun getTimelineTest() {
        activity().viewModel<TimelineFragmentViewModel> {
            getTimeline()

            await {
                assertEquals(timeline.value != null, true)

                if (timeline.value.isNullOrEmpty()) {
                    assertEquals(empty.value, true)
                } else {
                    assertEquals(empty.value, false)
                }
            }
        }
    }

    @Test
    fun getRepliesTest() {
        var item: TweetDto? = null
        activity().viewModel<TimelineFragmentViewModel> {
            ui {
                getTimeline()
            }

            await {
                assertEquals(timeline.value != null, true)

                if (timeline.value.isNullOrEmpty()) {
                    assertEquals(empty.value, true)
                } else {
                    assertEquals(empty.value, false)
                }
                timeline.value?.let {
                    item = it.last().getItem()
                }
            }
        }

        activity().viewModel<TweetDetailFragmentViewModel> {
            ui {
                setTweet(item)
                getReplies()
            }

            await {
                replies.value?.let {
                    assertEquals(it.isNullOrEmpty(), false)
                }
            }
        }
    }
}