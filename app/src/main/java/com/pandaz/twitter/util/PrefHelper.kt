package com.pandaz.twitter.util

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by LEE SU HO on 2019-04-30.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

private const val PREF_NAME = "TWITTER_PREF"
private const val KEY_TOKEN = "KEY_TOKEN"
private const val KEY_TOKEN_SECRET = "KEY_TOKEN_SECRET"

object PrefHelper {
    private lateinit var preferences: SharedPreferences

    fun init(context: Context) {
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }

    fun getToken() = preferences.getString(KEY_TOKEN, null)

    fun setToken(token: String) = setValue(KEY_TOKEN, token)

    fun getTokenSecret() = preferences.getString(KEY_TOKEN_SECRET, null)

    fun setTokenSecret(tokenSecret: String) = setValue(KEY_TOKEN_SECRET, tokenSecret)

    private fun setValue(key: String, value: String) {
        preferences.edit()?.let {
            it.putString(key, value)
            it.apply()
        }
    }
}