package com.pandaz.twitter.util

import android.app.Activity
import android.arch.lifecycle.*
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Handler
import android.os.Looper
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import com.pandaz.twitter.BR


/**
 * Created by LEE SU HO on 2019-04-30.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

infix fun <T> LiveData<T>.owner(owner: LifecycleOwner): ((T) -> Unit) -> Unit {
    return { block ->
        this.observe(owner, Observer { data ->
            data?.let { block(it) }
        })
    }
}

infix fun <T> (((T) -> Unit) -> Unit).observe(block: (T) -> Unit) {
    this.invoke(block)
}

infix fun <T> ((T) -> Unit)?.post(value: T) {
    Handler(Looper.getMainLooper()).post {
        this?.invoke(value)
    }
}

infix fun <T> ((T) -> Unit)?.call(value: T) = this?.invoke(value)

inline fun <reified T : Activity> Context.runActivity() = startActivity(Intent(this, T::class.java))

infix fun View.delayed(block: () -> Unit) = postDelayed(block, 1000L)

infix fun <T> MutableList<T>.swap(list: Collection<T>): () -> MutableList<T> {
    clear()
    addAll(list)
    return { this }
}

infix fun <T> (() -> T)?.action(block: T.() -> Unit) {
    if (this != null) {
        block(invoke())
    }
}

infix fun <T> List<T>?.valid(index: Int): (() -> T)? {
    if (this != null && index in 0 until size) {
        return { get(index) }
    }
    return null
}

infix fun <T> Array<T>?.valid(index: Int): (() -> T)? {
    if (this != null && index in 0 until size) {
        return { get(index) }
    }
    return null
}

fun Context.dp2px(dp: Int): Int {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), resources.displayMetrics).toInt()
}

infix fun SwipeRefreshLayout.refresh(block: () -> Unit) {
    setOnRefreshListener {
        block()
        isRefreshing = false
    }
}

inline fun <reified T : ViewModel> AppCompatActivity.viewModel(block: T.() -> Unit) = block(ViewModelProviders.of(this).get(T::class.java))

inline fun <reified T : ViewModel> Fragment.viewModel(block: T.() -> Unit) {
    val activity = activity
    activity?.let {
        block(ViewModelProviders.of(activity).get(T::class.java))
    }
}

fun AppCompatActivity.emptyFragments() = supportFragmentManager.fragments.size == 1

fun <T : ViewDataBinding> AppCompatActivity.binding(block: () -> Int): T {
    val binding = DataBindingUtil.setContentView<T>(this, block())
    binding.lifecycleOwner = this
    return binding
}

fun <T : ViewDataBinding> Fragment.binding(block: () -> Int): T {
    val binding = DataBindingUtil.inflate<T>(LayoutInflater.from(context), block(), null, false)
    binding.lifecycleOwner = this
    return binding
}

fun RecyclerView.layoutManager(block: () -> RecyclerView.LayoutManager) {
    layoutManager = block()
}

fun RecyclerView.adapter(block: () -> RecyclerView.Adapter<*>) {
    adapter = block()
}

fun RecyclerView.more(block: () -> Unit) {
    val manager = layoutManager
    if (manager is LinearLayoutManager) {
        addOnScrollListener(More(manager) { block() })
    }
}

infix fun <T> LiveData<T>.set(value: T?) {
    if (this is MutableLiveData) {
        setValue(value)
    }
}

infix fun ViewDataBinding.viewModel(block: () -> ViewModel) = setVariable(BR.viewModel, block())

fun Context.toast(block: () -> String) {
    Toast.makeText(this, block(), Toast.LENGTH_SHORT).show()
}
