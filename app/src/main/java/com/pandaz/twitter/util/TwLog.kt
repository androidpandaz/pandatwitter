package com.pandaz.twitter.util

import android.util.Log
import com.pandaz.twitter.BuildConfig

/**
 * Created by LEE SU HO on 2019-05-03.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

private const val TAG = "PADAZ_TWITTER"

object TwLog {

    private fun debug(block: () -> Unit) {
        if (BuildConfig.DEBUG) {
            block()
        }
    }

    fun log(message: String?) {
        debug {
            Log.d(TAG, message ?: "")
        }
    }

    fun log(error: Throwable?) {
        debug {
            Log.e(TAG, error?.toString() ?: "")
        }
    }
}