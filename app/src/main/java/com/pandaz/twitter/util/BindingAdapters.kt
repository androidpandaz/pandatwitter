package com.pandaz.twitter.util

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.pandaz.twitter.ui.base.BaseItemViewModel
import com.pandaz.twitter.ui.base.ItemAdapter
import com.pandaz.twitter.ui.detail.TweetDetailViewModel
import com.pandaz.twitter.ui.tweet.TweetItemViewModel

/**
 * Created by LEE SU HO on 2019-05-01.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */


@BindingAdapter("roundImage")
fun roundImage(view: ImageView, url: String?) {
    if (url.isNullOrEmpty()) {
        return
    }
    Glide.with(view)
        .load(url)
        .apply(
            RequestOptions().transform(
                CenterCrop(),
                RoundedCorners(view.context.dp2px(15))
            )
        )
        .transition(withCrossFade())
        .into(view)
}

@BindingAdapter("fullImage")
fun fullImage(view: ImageView, url: String?) {
    if (url.isNullOrEmpty()) {
        return
    }
    Glide.with(view)
        .load(url)
        .apply(
            RequestOptions().transform(
                CenterInside(),
                RoundedCorners(view.context.dp2px(15))
            )
        )
        .transition(withCrossFade())
        .into(view)
}

@BindingAdapter("circleImage")
fun circleImage(view: ImageView, url: String?) {
    if (url.isNullOrEmpty()) {
        return
    }
    Glide.with(view)
        .load(url)
        .apply(
            RequestOptions().transform(
                CenterCrop(),
                CircleCrop()
            )
        )
        .transition(withCrossFade())
        .into(view)
}

@Suppress("UNCHECKED_CAST")
@BindingAdapter("timeline")
fun setTimeline(view: RecyclerView, items: List<BaseItemViewModel<*>>?) {
    items?.let {
        val adapter = view.adapter as ItemAdapter<BaseItemViewModel<*>>
        adapter.update(it)
    }
}

@Suppress("UNCHECKED_CAST")
@BindingAdapter("detail", "replies", requireAll = true)
fun setDetails(view: RecyclerView, detail: TweetDetailViewModel?, replies: List<TweetItemViewModel>?) {
    val items = mutableListOf<BaseItemViewModel<*>>().apply {
        detail?.let { add(detail) }
        replies?.let { addAll(replies) }
    }

    val adapter = view.adapter as ItemAdapter<BaseItemViewModel<*>>
    adapter.update(items as List<Nothing>)
}