package com.pandaz.twitter.util

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.pandaz.twitter.R

/**
 * Created by LEE SU HO on 2019-05-01.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

object FragmentUtil {
    operator fun set(manager: FragmentManager?, id: Int, fragment: Fragment) {
        manager?.let {
            val transaction = it.beginTransaction()
            transaction.replace(id, fragment, fragment.javaClass.name)
            transaction.commitAllowingStateLoss()
        }
    }

    fun push(manager: FragmentManager?, containerResId: Int, fragment: Fragment) {
        manager?.let {
            val transaction = it.beginTransaction()
            transaction.setCustomAnimations(
                    R.anim.slide_in_right,
                    R.anim.slide_out_right,
                    R.anim.stay,
                    R.anim.slide_out_right)
            transaction.add(containerResId, fragment, fragment.javaClass.name)
            transaction.addToBackStack(fragment.javaClass.name)
            transaction.commitAllowingStateLoss()
        }
    }
}