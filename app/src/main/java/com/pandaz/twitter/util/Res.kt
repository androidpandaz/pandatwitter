package com.pandaz.twitter.util

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.support.annotation.ColorRes
import android.support.annotation.StringRes

/**
 * Created by LEE SU HO on 2019-05-01.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

@SuppressLint("StaticFieldLeak")
object Res {

    lateinit var context: Context

    fun init(appliction: Application) {
        context = appliction
    }

    @Suppress("DEPRECATION")
    fun getColor(@ColorRes id: Int) = context.resources.getColor(id)

    fun getString(@StringRes id: Int) = context.getString(id)

    fun getString(@StringRes id: Int, vararg args: Any?) = context.getString(id, *args)
}