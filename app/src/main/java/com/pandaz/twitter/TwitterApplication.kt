package com.pandaz.twitter

import android.app.Application
import com.pandaz.twitter.network.source.RemoteSource
import com.pandaz.twitter.util.PrefHelper
import com.pandaz.twitter.util.Res

/**
 * Created by LEE SU HO on 2019-04-30.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

private const val API_KEY = "9geJmVWZ0jCVgL3lygfucd2yz"
private const val SECRET_KEY = "0KMSCuNSvyXjQxoWyW5OyzUC0HjY804k3FYut9ZwFdXYpho3ZR"

class TwitterApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        Res.init(this)
        PrefHelper.init(this)
        RemoteSource.init(API_KEY, SECRET_KEY)
        RemoteSource.setToken(PrefHelper.getToken(), PrefHelper.getTokenSecret())
    }
}