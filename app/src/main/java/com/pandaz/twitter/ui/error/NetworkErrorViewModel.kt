package com.pandaz.twitter.ui.error

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.pandaz.twitter.R
import com.pandaz.twitter.network.lib.SUCCESS
import com.pandaz.twitter.network.lib.UNAUTHORIZED_ERROR_CODE
import com.pandaz.twitter.util.Res
import com.pandaz.twitter.util.set

/**
 * Created by LEE SU HO on 2019-05-05.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

class NetworkErrorViewModel : ViewModel() {

    val errorCode = MutableLiveData<String>()

    val errorMessage = MutableLiveData<String>()

    val retryMessage = MutableLiveData<String>()

    val empty = MutableLiveData<Boolean>()

    private var clickAuth: (() -> Unit)? = null

    private var clickNetwork: (() -> Unit)? = null

    fun click() {
        when (errorCode.value?.toInt()) {
            UNAUTHORIZED_ERROR_CODE -> clickAuth?.invoke()
            else -> clickNetwork?.invoke()
        }
    }

    fun setError(e: Pair<Int, String?>) {
        empty set (e.first == SUCCESS)
        errorCode set e.first.toString()
        errorMessage set e.second
        retryMessage set when (e.first) {
            UNAUTHORIZED_ERROR_CODE -> Res.getString(R.string.do_login)
            else -> Res.getString(R.string.retry)
        }
    }

    fun NetworkErrorViewModel.auth(block: () -> Unit) {
        clickAuth = block
    }

    fun NetworkErrorViewModel.network(block: () -> Unit) {
        clickNetwork = block
    }
}