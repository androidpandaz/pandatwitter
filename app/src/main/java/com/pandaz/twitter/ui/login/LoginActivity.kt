package com.pandaz.twitter.ui.login

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.pandaz.twitter.R
import com.pandaz.twitter.databinding.ActivityLoginBinding
import com.pandaz.twitter.network.lib.UNAUTHORIZED_ERROR_MESSAGE
import com.pandaz.twitter.ui.base.BaseActivity
import com.pandaz.twitter.ui.timeline.TimelineActivity
import com.pandaz.twitter.ui.title.title
import com.pandaz.twitter.util.*
import oauth.signpost.OAuth


class LoginActivity : BaseActivity() {

    val viewModel = LoginViewModel()

    val binding by lazy {
        binding<ActivityLoginBinding> { R.layout.activity_login }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding viewModel { viewModel }

        binding title {
            text { getString(R.string.title_login) }
            back { onBackPressed() }
        }

        viewModel.url owner this observe {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(it)))
        }

        viewModel.login owner this observe {
            if (it) {
                runActivity<TimelineActivity>()
                finish()
            } else {
                toast { UNAUTHORIZED_ERROR_MESSAGE }
            }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.data?.let {
            val verifier = it.getQueryParameter(OAuth.OAUTH_VERIFIER)
            if (verifier.isNullOrEmpty()) {
                toast { UNAUTHORIZED_ERROR_MESSAGE }
            } else {
                viewModel.accessToken(verifier)
            }
        }
    }
}
