package com.pandaz.twitter.ui.tweet

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import com.pandaz.twitter.R
import com.pandaz.twitter.network.dto.TweetDto
import com.pandaz.twitter.util.Res
import com.pandaz.twitter.util.TwLog
import com.pandaz.twitter.util.action
import com.pandaz.twitter.util.valid
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by LEE SU HO on 2019-05-03.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

interface TweetViewModel {

    fun getTweet(): TweetDto?

    private fun getData(): TweetDto? {
        return if (isReTweet()) {
            getTweet()?.reTweet
        } else {
            getTweet()
        }
    }

    fun isReTweet() = getTweet()?.reTweet != null

    fun getId() = getData()?.id

    private fun getMediaCount() = getData()?.extendedEntities?.media?.size ?: 0

    fun isEmptyMedia() = getMediaCount() != 0

    fun isMultiMedia() = getMediaCount() > 1

    fun getMediaUrl(index: Int): String? {
        val media = getData()?.extendedEntities?.media
        var result: String? = null
        media valid index action { result = mediaUrl }
        return result
    }

    fun getContent(): CharSequence {
        var text = getData()?.text
        if (text.isNullOrEmpty()) {
            return StringBuilder()
        }

        removeUrl { start, end -> text = text?.substring(start, end) }

        val builder = SpannableStringBuilder(text)

        try {
            hashtag { start, end -> builder.highlight(start, end) }

            mention { start, end -> builder.highlight(start, end) }

            url { start, end -> builder.highlight(start, end) }

        } catch (e: Exception) {
            TwLog.log(e)
        }
        return builder
    }

    private fun SpannableStringBuilder.highlight(start: Int, end: Int) {
        setSpan(ForegroundColorSpan(Res.getColor(R.color.twitterBlue)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    }

    private fun removeUrl(block: (start: Int, end: Int) -> Unit) {
        val text = getData()?.text
        if (text.isNullOrEmpty()) {
            return
        }
        getData()?.entities?.media?.forEach { media ->
            media.url?.let {
                val index = text.lastIndexOf(it)
                if (index != -1) {
                    block(0, index)
                }
            }
        }
    }

    private fun hashtag(block: (start: Int, end: Int) -> Unit) {
        val text = getData()?.text
        if (text.isNullOrEmpty()) {
            return
        }

        getData()?.entities?.hashtags?.forEach { hashtag ->
            val tag = "#${hashtag.text}"
            tag.toRegex().findAll(text).forEach {
                block(it.range.start, it.range.last + 1)
            }
        }
    }

    private fun url(block: (start: Int, end: Int) -> Unit) {
        val text = getData()?.text
        if (text.isNullOrEmpty()) {
            return
        }

        getData()?.entities?.urls?.forEach { data ->
            data.url?.let {
                val index = text.lastIndexOf(it)
                if (index != -1) {
                    block(index, index + it.length)
                }
            }
        }
    }

    private fun mention(block: (start: Int, end: Int) -> Unit) {
        val text = getData()?.text
        if (text.isNullOrEmpty()) {
            return
        }

        getData()?.entities?.mentions?.forEach { data ->
            data.name?.let {
                val keyword = "@$it"
                val index = text.lastIndexOf(keyword)
                if (index != -1) {
                    block(index, index + keyword.length)
                }
            }
        }
    }

    fun getProfileUrl() = getData()?.user?.profileUrl

    fun getProfileName() = getData()?.user?.name

    fun getProfileScreenName(): String? {
        val name = getData()?.user?.screenName
        return if (name.isNullOrEmpty()) {
            null
        } else {
            "@$name"
        }
    }

    fun getReTweetName() = getTweet()?.user?.name?.let {
        Res.getString(R.string.retweet_name, it)
    }


    fun getTime(): String? {
        val createAt = getData()?.createAt
        if (createAt.isNullOrEmpty()) {
            return null
        }

        val format = SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZZ yyyy", Locale.ENGLISH)
        val calendar = Calendar.getInstance()
        calendar.time = format.parse(createAt)
        val diff = System.currentTimeMillis() - calendar.timeInMillis
        val sec = (diff / 1000).toInt()
        val min = (diff / (1000 * 60)).toInt()
        val hour = (diff / (1000 * 60 * 60)).toInt()
        val day = (diff / (1000 * 60 * 60 * 24)).toInt()

        return when {
            day in 1..6 -> Res.getString(R.string.time_day, day)
            day == 0 && hour in 1..24 -> Res.getString(R.string.time_hour, hour)
            day == 0 && hour == 0 && min in 1..60 -> Res.getString(R.string.time_min, min)
            day == 0 && hour == 0 && min == 0 -> Res.getString(R.string.time_sec, sec)
            else -> " · ${calendar.get(Calendar.YEAR)} .${calendar.get(Calendar.MONTH) + 1} .${calendar.get(Calendar.DAY_OF_MONTH)}"
        }
    }

    private fun changeCount(count: Int) = when {
        count / 1000 >= 10 -> " ${count / 1000}K"
        else -> " $count"
    }

    fun getRetweetCount() = changeCount(getData()?.retweetCount ?: 0)

    fun getLikeCount() = changeCount(getData()?.favoriteCount ?: 0)
}