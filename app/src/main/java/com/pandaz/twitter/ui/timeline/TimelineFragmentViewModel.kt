package com.pandaz.twitter.ui.timeline

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.pandaz.twitter.network.dto.TweetDto
import com.pandaz.twitter.network.lib.SUCCESS
import com.pandaz.twitter.network.manager.TimelineManager
import com.pandaz.twitter.ui.tweet.TweetItemViewModel
import com.pandaz.twitter.util.set

/**
 * Created by LEE SU HO on 2019-05-01.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

class TimelineFragmentViewModel : ViewModel() {

    val timeline: LiveData<List<TweetItemViewModel>> = MutableLiveData()

    val click: LiveData<TweetDto> = MutableLiveData()

    val errors: LiveData<Pair<Int, String?>> = MutableLiveData()

    val empty: LiveData<Boolean> = MutableLiveData()

    fun getTimeline() {
        TimelineManager.getTimeline {
            success { data ->
                empty set data.isEmpty()
                timeline set data.map { createItem(it) }
                errors set Pair(SUCCESS, null)
            }

            error {
                errors set it
            }
        }
    }

    fun nextTimeline() {
        val oldList = timeline.value
        if (oldList.isNullOrEmpty()) {
            return
        }

        val item = oldList.last()

        item.getId()?.let { id ->
            TimelineManager.getTimeline(id) {
                success { data ->
                    val newList = data.map { createItem(it) }
                    timeline set ArrayList(oldList).apply { addAll(newList) }
                    errors set Pair(SUCCESS, null)
                }

                error {
                    if (timeline.value?.isEmpty() == true) {
                        errors set it
                    }
                }
            }
        }
    }

    private fun createItem(dto: TweetDto): TweetItemViewModel {
        return TweetItemViewModel.create {
            item { dto }
            clicked { click set dto }
        }
    }
}