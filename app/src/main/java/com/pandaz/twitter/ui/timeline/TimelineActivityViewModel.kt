package com.pandaz.twitter.ui.timeline

import android.arch.lifecycle.ViewModel
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.pandaz.twitter.util.FragmentUtil
import com.pandaz.twitter.util.action

/**
 * Created by LEE SU HO on 2019-05-01.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

class TimelineActivityViewModel : ViewModel() {

    private var layout: (() -> Int)? = null

    fun setFragment(manager: FragmentManager?, fragment: Fragment) {
        layout action {
            FragmentUtil.set(manager, this, fragment)
        }
    }

    fun pushFragment(manager: FragmentManager?, fragment: Fragment) {
        layout action {
            FragmentUtil.push(manager, this, fragment)
        }
    }

    fun TimelineActivityViewModel.layout(block: () -> Int) {
        layout = block
    }
}