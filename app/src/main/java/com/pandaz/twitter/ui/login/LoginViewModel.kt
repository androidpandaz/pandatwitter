package com.pandaz.twitter.ui.login

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.pandaz.twitter.network.manager.AuthManager
import com.pandaz.twitter.util.set

/**
 * Created by LEE SU HO on 2019-05-01.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

class LoginViewModel : ViewModel() {

    val url = MutableLiveData<String>()
    val login = MutableLiveData<Boolean>()

    fun login() = requestToken()

    private fun requestToken() {
        AuthManager.requestToken {

            success { url set it }

            error { url set null }
        }
    }

    fun accessToken(verifier: String) {
        AuthManager.accessToken(verifier) {

            success { login set true }

            error { login set false }
        }
    }
}