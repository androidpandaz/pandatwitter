package com.pandaz.twitter.ui.detail

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.pandaz.twitter.network.dto.TweetDto
import com.pandaz.twitter.network.manager.ReplyManager
import com.pandaz.twitter.ui.tweet.TweetItemViewModel
import com.pandaz.twitter.util.set

/**
 * Created by LEE SU HO on 2019-05-03.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

class TweetDetailFragmentViewModel : ViewModel() {

    val detail = MutableLiveData<TweetDetailViewModel>()

    val replies = MutableLiveData<List<TweetItemViewModel>>()

    val click = MutableLiveData<TweetDto>()

    var screenName: String? = null

    fun setTweet(dto: TweetDto?) {
        detail set TweetDetailViewModel().apply {
            setTweet(dto)
            screenName = getProfileScreenName()
        }
    }

    fun getReplies() {
        val name = screenName
        if (name.isNullOrEmpty()) {
            return
        }

        ReplyManager.getReplys(name) {
            success {
                createItems(it.statuses)
            }

            error {
                // Do nothing
            }
        }
    }

    fun nextReplies() {
        val name = screenName
        if (name.isNullOrEmpty()) {
            return
        }

        val oldList = replies.value
        if (oldList.isNullOrEmpty()) {
            return
        }

        val item = oldList[oldList.lastIndex]
        item.getId()?.let { id ->
            ReplyManager.getReplys(name, id) {
                success {
                    it.statuses?.let { list ->
                        if (list.isNullOrEmpty() || id == list.last().id) {
                            return@success
                        }
                    }

                    createItems(it.statuses)
                }

                error {
                    // Do nothing
                }
            }
        }
    }

    private fun createItem(dto: TweetDto): TweetItemViewModel {
        return TweetItemViewModel.create {
            item { dto }
            clicked { click set dto }
        }
    }

    private fun createItems(dtos: List<TweetDto>? = null) {
        val oldList = replies.value
        replies set mutableListOf<TweetItemViewModel>().apply {
            if (!oldList.isNullOrEmpty()) {
                addAll(oldList)
            }

            dtos?.let {
                it.forEach { dto -> add(createItem(dto)) }
            }
        }
    }
}