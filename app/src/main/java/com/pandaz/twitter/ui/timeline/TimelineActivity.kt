package com.pandaz.twitter.ui.timeline

import android.os.Bundle
import com.pandaz.twitter.R
import com.pandaz.twitter.databinding.ActivityTimelineBinding
import com.pandaz.twitter.ui.base.BaseActivity
import com.pandaz.twitter.ui.title.TitleViewModel
import com.pandaz.twitter.util.Res
import com.pandaz.twitter.util.binding
import com.pandaz.twitter.util.emptyFragments
import com.pandaz.twitter.util.viewModel


class TimelineActivity : BaseActivity() {

    private val binding by lazy {
        binding<ActivityTimelineBinding> { R.layout.activity_timeline }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }

    private fun init() {

        viewModel<TitleViewModel> {
            binding.titleViewModel = this
        }

        viewModel<TimelineActivityViewModel> {

            layout { R.id.timelineLayout }

            binding.viewModel = this

            setFragment(supportFragmentManager, TimelineFragment.newInstance())
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()

        viewModel<TitleViewModel> {
            if (emptyFragments()) {
                text { Res.getString(R.string.title_home) }
                hideBacked()
            } else {
                text { Res.getString(R.string.title_tweet) }
                back { onBackPressed() }
            }
        }
    }
}
