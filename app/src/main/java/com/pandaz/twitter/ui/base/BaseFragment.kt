package com.pandaz.twitter.ui.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pandaz.twitter.util.TwLog

/**
 * Created by LEE SU HO on 2019-05-01.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

open class BaseFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        TwLog.log("onCreateView: ${this::class.java.name}")
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        TwLog.log("onDestroyView: ${this::class.java.name}")
    }
}