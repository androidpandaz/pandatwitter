package com.pandaz.twitter.ui.timeline

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pandaz.twitter.R
import com.pandaz.twitter.databinding.LayoutTimelineBinding
import com.pandaz.twitter.ui.base.BaseFragment
import com.pandaz.twitter.ui.base.ItemAdapter
import com.pandaz.twitter.ui.detail.TweetDetailFragment
import com.pandaz.twitter.ui.error.NetworkErrorViewModel
import com.pandaz.twitter.ui.login.LoginActivity
import com.pandaz.twitter.ui.title.TitleViewModel
import com.pandaz.twitter.ui.tweet.TweetItemViewModel
import com.pandaz.twitter.util.*


/**
 * Created by LEE SU HO on 2019-05-01.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

class TimelineFragment : BaseFragment() {

    private val binding by lazy {
        binding<LayoutTimelineBinding> { R.layout.layout_timeline }
    }

    private val adapter = ItemAdapter<TweetItemViewModel> { R.layout.item_timeline }

    val viewModel = TimelineFragmentViewModel()

    companion object {
        fun newInstance() = TimelineFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        init()
        load()
        return binding.root
    }

    private fun init() {
        binding viewModel { viewModel }

        binding.recyclerView.apply {
            layoutManager { LinearLayoutManager(context) }

            adapter { this@TimelineFragment.adapter }

            more { viewModel.nextTimeline() }
        }

        binding.refreshLayout refresh {
            viewModel.getTimeline()
        }

        viewModel.click owner this observe {
            viewModel<TimelineActivityViewModel> {
                pushFragment(fragmentManager, TweetDetailFragment.newInstance(it))
            }
        }

        viewModel.errors owner this observe {
            viewModel<NetworkErrorViewModel> { this.setError(it) }
        }

        viewModel<NetworkErrorViewModel> {
            binding.networkViewModel = this

            network {
                viewModel.getTimeline()
            }

            auth {
                activity?.apply {
                    runActivity<LoginActivity>()
                    finish()
                }
            }
        }

        viewModel<TitleViewModel> {
            text { Res.getString(R.string.title_home) }
        }
    }

    private fun load() = viewModel.getTimeline()
}