package com.pandaz.twitter.ui.base

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pandaz.twitter.BR
import com.pandaz.twitter.util.action
import com.pandaz.twitter.util.swap

/**
 * Created by LEE SU HO on 2019-05-02.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

class BindingViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val binding: ViewDataBinding? = DataBindingUtil.bind(view)
}

class ItemAdapter<VM : BaseItemViewModel<*>>(private val block: (VM) -> Int) : RecyclerView.Adapter<BindingViewHolder>() {
    private val list = mutableListOf<VM>()

    override fun onCreateViewHolder(parent: ViewGroup, layoutId: Int): BindingViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(layoutId, parent, false)
        return BindingViewHolder(view)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(viewHolder: BindingViewHolder, positon: Int) {
        viewHolder.binding?.setVariable(BR.viewModel, list[positon])
        viewHolder.binding?.executePendingBindings()
    }

    fun update(items: List<VM>) {
        list swap items action { notifyDataSetChanged() }
    }

    override fun getItemViewType(position: Int) = block(list[position])
}