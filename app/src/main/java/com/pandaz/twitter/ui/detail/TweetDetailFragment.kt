package com.pandaz.twitter.ui.detail

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pandaz.twitter.R
import com.pandaz.twitter.databinding.LayoutTweetDetailBinding
import com.pandaz.twitter.network.dto.TweetDto
import com.pandaz.twitter.ui.base.BaseFragment
import com.pandaz.twitter.ui.base.BaseItemViewModel
import com.pandaz.twitter.ui.base.ItemAdapter
import com.pandaz.twitter.ui.timeline.TimelineActivityViewModel
import com.pandaz.twitter.ui.title.TitleViewModel
import com.pandaz.twitter.util.*

/**
 * Created by LEE SU HO on 2019-05-02.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

class TweetDetailFragment : BaseFragment() {

    val binding by lazy {
        binding<LayoutTweetDetailBinding> { R.layout.layout_tweet_detail }
    }

    val viewModel = TweetDetailFragmentViewModel()

    private val adapter = ItemAdapter<BaseItemViewModel<*>> {
        when (it) {
            is TweetDetailViewModel -> R.layout.item_tweet_detail
            else -> R.layout.item_timeline
        }
    }

    companion object {
        private const val MAIN_CONTENT = "MAIN_CONTENT"

        fun newInstance(dto: TweetDto?) = TweetDetailFragment().apply {
            arguments = Bundle().apply {
                putParcelable(MAIN_CONTENT, dto)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        init()
        load()
        return binding.root
    }

    private fun init() {

        viewModel.setTweet(getContent())

        binding viewModel { viewModel }

        binding.recyclerView.apply {
            layoutManager { LinearLayoutManager(context) }

            adapter { this@TweetDetailFragment.adapter }

            more { viewModel.nextReplies() }
        }

        viewModel.click owner this@TweetDetailFragment observe {
            viewModel<TimelineActivityViewModel> { pushFragment(fragmentManager, newInstance(it)) }
        }

        viewModel<TitleViewModel> {
            text { Res.getString(R.string.title_tweet) }
            back { activity?.onBackPressed() }
        }
    }

    private fun getContent() = arguments?.getParcelable<TweetDto>(MAIN_CONTENT)

    private fun load() {
        viewModel.getReplies()
    }
}