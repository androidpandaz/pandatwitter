package com.pandaz.twitter.ui.tweet

import com.pandaz.twitter.network.dto.TweetDto
import com.pandaz.twitter.ui.base.BaseItemViewModel

/**
 * Created by LEE SU HO on 2019-05-01.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

class TweetItemViewModel : BaseItemViewModel<TweetDto>(), TweetViewModel {

    companion object {

        fun create(block: TweetItemViewModel.() -> Unit) = TweetItemViewModel().apply { block(this) }
    }

    override fun getTweet() = getItem()
}