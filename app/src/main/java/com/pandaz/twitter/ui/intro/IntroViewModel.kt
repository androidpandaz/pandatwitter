package com.pandaz.twitter.ui.intro

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.pandaz.twitter.util.PrefHelper
import com.pandaz.twitter.util.set

/**
 * Created by LEE SU HO on 2019-05-01.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

class IntroViewModel : ViewModel() {

    val isLogin = MutableLiveData<Boolean>()

    fun checkLogin() {
        isLogin set (!PrefHelper.getToken().isNullOrEmpty() && !PrefHelper.getTokenSecret().isNullOrEmpty())
    }

}