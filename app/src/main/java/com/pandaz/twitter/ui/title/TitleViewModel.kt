package com.pandaz.twitter.ui.title

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.ViewDataBinding
import com.pandaz.twitter.BR
import com.pandaz.twitter.util.set

/**
 * Created by LEE SU HO on 2019-05-04.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

infix fun ViewDataBinding.title(block: TitleViewModel.() -> Unit) {
    val viewModel = TitleViewModel()
    block(viewModel)
    setVariable(BR.titleViewModel, viewModel)
}

class TitleViewModel : ViewModel() {

    private var back: (() -> Unit)? = null

    val title = MutableLiveData<String>()

    val isBacked = MutableLiveData<Boolean>()

    fun TitleViewModel.back(block: () -> Unit) {
        back = block
        isBacked set true
    }

    fun TitleViewModel.text(block: () -> String) {
        title set block()
    }

    fun clickBack() = back?.invoke()

    fun hideBacked() {
        isBacked set false
    }
}