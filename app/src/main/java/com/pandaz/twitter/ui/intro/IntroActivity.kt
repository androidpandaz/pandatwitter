package com.pandaz.twitter.ui.intro

import android.os.Bundle
import com.pandaz.twitter.R
import com.pandaz.twitter.databinding.ActivityIntroBinding
import com.pandaz.twitter.ui.base.BaseActivity
import com.pandaz.twitter.ui.login.LoginActivity
import com.pandaz.twitter.ui.timeline.TimelineActivity
import com.pandaz.twitter.util.*


class IntroActivity : BaseActivity() {

    private val binding by lazy {
        binding<ActivityIntroBinding> { R.layout.activity_intro }
    }

    private val viewModel = IntroViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }

    private fun init() {
        viewModel.isLogin owner this observe {
            if (it) {
                runActivity<TimelineActivity>()
            } else {
                runActivity<LoginActivity>()
            }
            finish()
        }

        binding.root delayed { viewModel.checkLogin() }
    }

    override fun isWhiteStatus() = false
}
