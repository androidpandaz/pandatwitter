package com.pandaz.twitter.ui.detail

import android.arch.lifecycle.MutableLiveData
import com.pandaz.twitter.network.dto.TweetDto
import com.pandaz.twitter.ui.base.BaseItemViewModel
import com.pandaz.twitter.ui.tweet.TweetViewModel
import com.pandaz.twitter.util.set

/**
 * Created by LEE SU HO on 2019-05-03.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

class TweetDetailViewModel : BaseItemViewModel<TweetDto>(), TweetViewModel {

    private val tweet = MutableLiveData<TweetDto>()

    override fun getTweet() = tweet.value

    fun setTweet(dto: TweetDto?) {
        tweet set dto
    }
}