package com.pandaz.twitter.ui.base

import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.pandaz.twitter.util.TwLog

/**
 * Created by LEE SU HO on 2019-05-02.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        TwLog.log("onCreate: ${this::class.java.name}")
        setStatusTextColor(!isWhiteStatus())
    }

    override fun onDestroy() {
        super.onDestroy()
        TwLog.log("onDestroy: ${this::class.java.name}")
    }

    private fun setStatusTextColor(white: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val flags = window.decorView.systemUiVisibility
            window.decorView.systemUiVisibility =
                if (white) flags and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv() else flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
    }

    protected open fun isWhiteStatus() = true
}