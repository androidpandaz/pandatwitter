package com.pandaz.twitter.ui.base

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.pandaz.twitter.util.set

/**
 * Created by LEE SU HO on 2019-05-02.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

open class BaseItemViewModel<ITEM> : ViewModel() {

    private val item = MutableLiveData<ITEM>()

    private var clicked: (() -> Unit)? = null

    open fun getItem() = item.value

    fun BaseItemViewModel<ITEM>.item(block: () -> ITEM) {
        item set block()
    }

    fun BaseItemViewModel<ITEM>.clicked(block: () -> Unit) {
        clicked = block
    }

    fun click() = clicked?.invoke()

}