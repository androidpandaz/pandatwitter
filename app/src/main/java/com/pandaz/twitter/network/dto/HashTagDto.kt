package com.pandaz.twitter.network.dto

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by LEE SU HO on 2019-05-02.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

data class HashTagDto(

    val text: String?,
    val indices: List<Int>?

) : Parcelable {
    constructor(source: Parcel) : this(
        source.readString(),
        ArrayList<Int>().apply { source.readList(this, Int::class.java.classLoader) }
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(text)
        writeList(indices)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<HashTagDto> = object : Parcelable.Creator<HashTagDto> {
            override fun createFromParcel(source: Parcel): HashTagDto = HashTagDto(source)
            override fun newArray(size: Int): Array<HashTagDto?> = arrayOfNulls(size)
        }
    }
}