package com.pandaz.twitter.network.dto

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by LEE SU HO on 2019-05-01.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

class TweetDto(

        val id: Long?,
        val text: String?,
        val entities: EntitiesDto?,
        val user: UserDto?,
        @SerializedName("retweeted_status")
        val reTweet: TweetDto?,
        @SerializedName("created_at")
        val createAt: String?,
        @SerializedName("extended_entities")
        val extendedEntities: EntitiesDto?,
        @SerializedName("retweet_count")
        val retweetCount: Int?,
        @SerializedName("favorite_count")
        val favoriteCount: Int?

) : Parcelable {
    constructor(source: Parcel) : this(
            source.readValue(Long::class.java.classLoader) as Long?,
            source.readString(),
            source.readParcelable<EntitiesDto>(EntitiesDto::class.java.classLoader),
            source.readParcelable<UserDto>(UserDto::class.java.classLoader),
            source.readParcelable<TweetDto>(TweetDto::class.java.classLoader),
            source.readString(),
            source.readParcelable<EntitiesDto>(EntitiesDto::class.java.classLoader),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeValue(id)
        writeString(text)
        writeParcelable(entities, 0)
        writeParcelable(user, 0)
        writeParcelable(reTweet, 0)
        writeString(createAt)
        writeParcelable(extendedEntities, 0)
        writeValue(retweetCount)
        writeValue(favoriteCount)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<TweetDto> = object : Parcelable.Creator<TweetDto> {
            override fun createFromParcel(source: Parcel): TweetDto = TweetDto(source)
            override fun newArray(size: Int): Array<TweetDto?> = arrayOfNulls(size)
        }
    }
}
