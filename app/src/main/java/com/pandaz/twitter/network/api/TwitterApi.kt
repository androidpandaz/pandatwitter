package com.pandaz.twitter.network.api

import com.pandaz.twitter.network.dto.RepliesDto
import com.pandaz.twitter.network.dto.TweetDto
import com.pandaz.twitter.network.lib.BaseUrl
import com.pandaz.twitter.network.lib.Call
import com.pandaz.twitter.network.lib.GET
import com.pandaz.twitter.network.lib.Query

/**
 * Created by LEE SU HO on 2019-05-01.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

@BaseUrl("https://api.twitter.com")
interface TwitterApi {

    @GET("/1.1/statuses/home_timeline.json")
    fun getTimeline(@Query("max_id") maxId: Long? = null): Call<List<TweetDto>>?

    @GET("/1.1/search/tweets.json")
    fun getReplys(@Query("q") query: String? = null,
                  @Query("max_id") maxId: Long? = null): Call<RepliesDto>?
}