package com.pandaz.twitter.network.dto

/**
 * Created by LEE SU HO on 2019-05-03.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

data class RepliesDto(
        val statuses: List<TweetDto>?
)