package com.pandaz.twitter.network.dto

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by LEE SU HO on 2019-05-02.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

data class MediaDto(

    val id: Long?,
    val url: String?,
    @SerializedName("media_url_https")
    val mediaUrl: String?

) : Parcelable {
    constructor(source: Parcel) : this(
        source.readValue(Long::class.java.classLoader) as Long?,
        source.readString(),
        source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeValue(id)
        writeString(url)
        writeString(mediaUrl)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<MediaDto> = object : Parcelable.Creator<MediaDto> {
            override fun createFromParcel(source: Parcel): MediaDto = MediaDto(source)
            override fun newArray(size: Int): Array<MediaDto?> = arrayOfNulls(size)
        }
    }
}
