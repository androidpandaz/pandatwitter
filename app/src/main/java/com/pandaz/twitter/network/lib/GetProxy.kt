package com.pandaz.twitter.network.lib

import com.google.gson.Gson
import com.pandaz.twitter.util.TwLog
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

/**
 * Created by LEE SU HO on 2019-05-04.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

class GetProxy(private val holder: RestHolder) {

    @Suppress("UNCHECKED_CAST")
    fun <T> request(callback: Callback<T>) {
        try {
            val url = URL(makeUrl())
            val connection = url.openConnection() as HttpURLConnection
            val httpMethod = holder.method?.getAnnotation(GET::class.java)

            connection.requestMethod = httpMethod?.type
            holder.auth?.consumer?.sign(connection)

            val code = connection.responseCode
            val message = connection.responseMessage
            TwLog.log("$code ${connection.responseMessage} $url")

            if (code == HttpURLConnection.HTTP_OK) {
                val body = readStream(connection.inputStream)
                val json = Gson().fromJson<Any>("{\"data\"=$body}", holder.method?.genericReturnType)
                callback.success((json as Call<*>).data as T)
            } else {
                callback.error(Pair(code, message))
            }

        } catch (e: Exception) {
            callback.error(Pair(NETWORK_ERROR_CODE, NETWORK_ERROR_MESSAGE))
            TwLog.log(e)
        }
    }

    private fun makeUrl(): String? {
        if (holder.url.isNullOrEmpty()) {
            return null
        }

        val url = StringBuilder(holder.url!!)
        val params = holder.params
        if (!params.isNullOrEmpty()) {
            url.append("?")
            params.forEachIndexed { index, pair ->
                if (index != 0) {
                    url.append("&")
                }
                url.append("${pair.first}=${pair.second}")
            }
        }
        return url.toString()
    }

    private fun readStream(input: InputStream): String {
        var reader: BufferedReader? = null
        val response = StringBuffer()
        try {
            reader = BufferedReader(InputStreamReader(input))
            var line: String?
            while (true) {
                line = reader.readLine()
                if (line == null) {
                    break
                }
                response.append(line)
            }
        } finally {
            reader?.let { reader.close() }
        }
        return response.toString()
    }
}