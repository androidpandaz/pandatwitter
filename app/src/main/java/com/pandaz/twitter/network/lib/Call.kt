package com.pandaz.twitter.network.lib

/**
 * Created by LEE SU HO on 2019-05-01.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

class Call<T> {
    var data: T? = null

    @Transient
    var holder: RestHolder? = null

    fun <T> request(callback: Callback<T>) {
        holder?.let { GetProxy(it).request(callback) }
    }
}