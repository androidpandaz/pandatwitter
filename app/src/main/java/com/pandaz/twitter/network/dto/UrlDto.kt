package com.pandaz.twitter.network.dto

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by LEE SU HO on 2019-05-02.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

data class UrlDto(

    val url: String?

) : Parcelable {
    constructor(source: Parcel) : this(
        source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(url)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<UrlDto> = object : Parcelable.Creator<UrlDto> {
            override fun createFromParcel(source: Parcel): UrlDto = UrlDto(source)
            override fun newArray(size: Int): Array<UrlDto?> = arrayOfNulls(size)
        }
    }
}