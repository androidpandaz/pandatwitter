package com.pandaz.twitter.network.lib

/**
 * Created by LEE SU HO on 2019-05-01.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

const val SUCCESS = 200
const val UNAUTHORIZED_ERROR_CODE = 401
const val NETWORK_ERROR_CODE = 502

const val UNAUTHORIZED_ERROR_MESSAGE = "인증 오류가 발생했습니다."
const val NETWORK_ERROR_MESSAGE = "네트워크 오류가 발생했습니다."