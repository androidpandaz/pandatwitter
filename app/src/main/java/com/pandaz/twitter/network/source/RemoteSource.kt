package com.pandaz.twitter.network.source

import com.pandaz.twitter.network.api.TwitterApi
import com.pandaz.twitter.network.lib.OAuthHolder
import com.pandaz.twitter.network.lib.SimpleNetwork

/**
 * Created by LEE SU HO on 2019-04-29.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

object RemoteSource {
    private val networks = SimpleNetwork()

    val twitterApi = networks.getApi<TwitterApi>()

    fun init(apiKey: String, secretKey: String) {
        networks.auth(apiKey, secretKey)
    }

    fun setToken(token: String?, tokenSecret: String?) {
        networks.oauth?.apply {
            consumer.setTokenWithSecret(token, tokenSecret)
            provider.isOAuth10a = true
        }
    }

    fun oauth(block: (OAuthHolder) -> Unit) = RemoteSource.networks.oauth?.let { block(it) }
}