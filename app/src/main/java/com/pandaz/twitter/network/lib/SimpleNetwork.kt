package com.pandaz.twitter.network.lib

import java.lang.reflect.Method
import java.lang.reflect.Proxy.newProxyInstance


/**
 * Created by LEE SU HO on 2019-04-30.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */
class SimpleNetwork {

    var oauth: OAuthHolder? = null

    inline fun <reified T> getApi(): T {
        return newProxyInstance(T::class.java.classLoader, arrayOf(T::class.java)) { _, method, args ->
            return@newProxyInstance CallFactory.create<T>(method, args, oauth)
        } as T
    }

    fun auth(apiKey: String, secretKey: String) {
        oauth = OAuthHolder(apiKey, secretKey)
    }
}

interface Callback<T> {
    fun success(data: T)
    fun error(e: Pair<Int, String?>)
}

class RestHolder {
    var url: String? = null
    var method: Method? = null
    var auth: OAuthHolder? = null
    var params: MutableList<Pair<String, String>>? = null
}




