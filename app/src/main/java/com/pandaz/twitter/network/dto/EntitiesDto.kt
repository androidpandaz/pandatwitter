package com.pandaz.twitter.network.dto

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by LEE SU HO on 2019-05-02.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

data class EntitiesDto(

    val hashtags: List<HashTagDto>?,
    val media: List<MediaDto>?,
    val urls: List<UrlDto>?,
    @SerializedName("user_mentions")
    val mentions: List<MentionDto>?

) : Parcelable {
    constructor(source: Parcel) : this(
        ArrayList<HashTagDto>().apply { source.readList(this, HashTagDto::class.java.classLoader) },
        ArrayList<MediaDto>().apply { source.readList(this, MediaDto::class.java.classLoader) },
        ArrayList<UrlDto>().apply { source.readList(this, UrlDto::class.java.classLoader) },
        ArrayList<MentionDto>().apply { source.readList(this, MentionDto::class.java.classLoader) }
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeList(hashtags)
        writeList(media)
        writeList(urls)
        writeList(mentions)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<EntitiesDto> = object : Parcelable.Creator<EntitiesDto> {
            override fun createFromParcel(source: Parcel): EntitiesDto = EntitiesDto(source)
            override fun newArray(size: Int): Array<EntitiesDto?> = arrayOfNulls(size)
        }
    }
}
