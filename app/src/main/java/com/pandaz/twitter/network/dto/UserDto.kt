package com.pandaz.twitter.network.dto

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by LEE SU HO on 2019-05-02.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

data class UserDto(

    val id: Long?,
    val name: String?,
    @SerializedName("screen_name")
    val screenName: String?,
    @SerializedName("profile_image_url_https")
    val profileUrl: String?

) : Parcelable {
    constructor(source: Parcel) : this(
        source.readValue(Long::class.java.classLoader) as Long?,
        source.readString(),
        source.readString(),
        source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeValue(id)
        writeString(name)
        writeString(screenName)
        writeString(profileUrl)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<UserDto> = object : Parcelable.Creator<UserDto> {
            override fun createFromParcel(source: Parcel): UserDto = UserDto(source)
            override fun newArray(size: Int): Array<UserDto?> = arrayOfNulls(size)
        }
    }
}