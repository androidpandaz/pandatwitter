package com.pandaz.twitter.network.lib

import java.lang.reflect.Method

/**
 * Created by LEE SU HO on 2019-05-01.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */


object CallFactory {

    inline fun <reified T> create(method: Method, args: Array<Any?>?, oauth: OAuthHolder?): Call<*> {
        val httpMethod = method.getAnnotation(GET::class.java)
        val baseUrl = T::class.java.getAnnotation(BaseUrl::class.java)
        val url = baseUrl?.value + httpMethod?.value
        return GET(url, method, args, oauth)
    }

    @Suppress("UNCHECKED_CAST")
    fun GET(url: String, method: Method, args: Array<Any?>?, oauth: OAuthHolder?): Call<*> {
        return Call<Any>().apply {
            holder = RestHolder().apply {
                this.url = url
                this.method = method
                this.auth = oauth
                this.params = mutableListOf()
                args?.forEachIndexed { index, param ->
                    val query = method.parameterAnnotations[index][0] as Query?
                    if (query != null && param != null) {
                        this.params?.add(Pair(query.value, param.toString()))
                    }
                }
            }
        }
    }
}