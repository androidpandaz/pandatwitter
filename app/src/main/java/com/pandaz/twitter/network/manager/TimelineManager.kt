package com.pandaz.twitter.network.manager

import com.pandaz.twitter.network.RestHolder
import com.pandaz.twitter.network.delegate
import com.pandaz.twitter.network.dto.TweetDto
import com.pandaz.twitter.network.request
import com.pandaz.twitter.network.source.RemoteSource

/**
 * Created by LEE SU HO on 2019-04-30.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

object TimelineManager {

    fun getTimeline(block: RestHolder<List<TweetDto>>.() -> Unit) {
        request(block) {
            RemoteSource.twitterApi.getTimeline()?.request(delegate(it))
        }
    }

    fun getTimeline(maxId: Long, block: RestHolder<List<TweetDto>>.() -> Unit) {
        request(block) {
            RemoteSource.twitterApi.getTimeline(maxId)?.request(delegate(it))
        }
    }
}