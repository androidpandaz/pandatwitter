package com.pandaz.twitter.network

import com.pandaz.twitter.network.lib.Callback
import com.pandaz.twitter.util.call
import com.pandaz.twitter.util.post
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

/**
 * Created by LEE SU HO on 2019-05-01.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

class RestHolder<T> {
    var success: ((T) -> Unit)? = null
    var error: ((Pair<Int, String?>) -> Unit)? = null
    var start: (() -> Unit)? = null
    var finish: (() -> Unit)? = null

    fun RestHolder<T>.success(block: (T) -> Unit) {
        success = { block post it }
    }

    fun RestHolder<T>.error(block: (Pair<Int, String?>) -> Unit) {
        error = { block post it }
    }

    fun RestHolder<T>.start(block: () -> Unit) {
        start = block
    }

    fun RestHolder<T>.finish(block: () -> Unit) {
        finish = block
    }
}

fun <T> request(handler: RestHolder<T>.() -> Unit, run: (RestHolder<T>) -> Unit) {
    val holder = RestHolder<T>()
    handler(holder)

    CoroutineScope(Dispatchers.Main).launch {
        holder.start?.invoke()
        CoroutineScope(Dispatchers.Default).async { run(holder) }.await()
        holder.finish?.invoke()
    }
}

fun <T> delegate(holder: RestHolder<T>) = object : Callback<T> {
    override fun success(data: T) {
        holder.success call data
    }

    override fun error(e: Pair<Int, String?>) {
        holder.error call e
    }
}