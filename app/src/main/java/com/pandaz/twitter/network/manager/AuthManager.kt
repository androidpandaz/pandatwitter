package com.pandaz.twitter.network.manager

import com.pandaz.twitter.network.RestHolder
import com.pandaz.twitter.network.lib.UNAUTHORIZED_ERROR_CODE
import com.pandaz.twitter.network.lib.UNAUTHORIZED_ERROR_MESSAGE
import com.pandaz.twitter.network.request
import com.pandaz.twitter.network.source.RemoteSource
import com.pandaz.twitter.util.PrefHelper
import com.pandaz.twitter.util.TwLog
import com.pandaz.twitter.util.call

/**
 * Created by LEE SU HO on 2019-04-30.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

private const val CALLBACK_URL = "pandaz://"

object AuthManager {
    fun requestToken(block: RestHolder<String>.() -> Unit) {
        request(block) { holder ->
            try {
                RemoteSource.oauth {
                    val url = it.provider.retrieveRequestToken(it.consumer, CALLBACK_URL)
                    if (url.isNullOrEmpty()) {
                        holder.error call Pair(UNAUTHORIZED_ERROR_CODE, UNAUTHORIZED_ERROR_MESSAGE)
                    } else {
                        holder.success call url
                    }
                }
            } catch (e: Throwable) {
                TwLog.log(e)
                holder.error call Pair(UNAUTHORIZED_ERROR_CODE, UNAUTHORIZED_ERROR_MESSAGE)
            }
        }
    }

    fun accessToken(verifier: String, block: RestHolder<Unit>.() -> Unit) {
        request(block) { holder ->
            try {
                RemoteSource.oauth {
                    it.provider.retrieveAccessToken(it.consumer, verifier)
                    PrefHelper.setToken(it.consumer.token)
                    PrefHelper.setTokenSecret(it.consumer.tokenSecret)
                }
                holder.success call Unit
            } catch (e: Throwable) {
                TwLog.log(e)
                holder.error call Pair(UNAUTHORIZED_ERROR_CODE, UNAUTHORIZED_ERROR_MESSAGE)
            }
        }
    }
}