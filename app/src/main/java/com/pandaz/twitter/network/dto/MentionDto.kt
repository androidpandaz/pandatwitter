package com.pandaz.twitter.network.dto

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by LEE SU HO on 2019-05-02.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

class MentionDto(

    @SerializedName("screen_name")
    val name: String?

) : Parcelable {
    constructor(source: Parcel) : this(
        source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(name)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<MentionDto> = object : Parcelable.Creator<MentionDto> {
            override fun createFromParcel(source: Parcel): MentionDto = MentionDto(source)
            override fun newArray(size: Int): Array<MentionDto?> = arrayOfNulls(size)
        }
    }
}