package com.pandaz.twitter.network.lib

import oauth.signpost.basic.DefaultOAuthConsumer
import oauth.signpost.basic.DefaultOAuthProvider

/**
 * Created by LEE SU HO on 2019-04-30.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */


class OAuthHolder(apiKey: String, secretKey: String) {

    var consumer = DefaultOAuthConsumer(apiKey, secretKey)

    var provider = DefaultOAuthProvider(
        "https://api.twitter.com/oauth/request_token",
        "https://api.twitter.com/oauth/access_token",
        "https://api.twitter.com/oauth/authorize"
    )
}