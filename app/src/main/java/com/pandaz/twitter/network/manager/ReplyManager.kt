package com.pandaz.twitter.network.manager

import com.pandaz.twitter.network.RestHolder
import com.pandaz.twitter.network.delegate
import com.pandaz.twitter.network.dto.RepliesDto
import com.pandaz.twitter.network.request
import com.pandaz.twitter.network.source.RemoteSource
import java.net.URLEncoder

/**
 * Created by LEE SU HO on 2019-04-30.
 * Copyright © 2019 androidpandaz. All rights reserved.
 */

object ReplyManager {

    fun getReplys(name: String, block: RestHolder<RepliesDto>.() -> Unit) {
        request(block) {
            RemoteSource.twitterApi.getReplys(URLEncoder.encode("to:$name", "utf-8"))?.request(delegate(it))
        }
    }

    fun getReplys(name: String, maxId: Long, block: RestHolder<RepliesDto>.() -> Unit) {
        request(block) {
            RemoteSource.twitterApi.getReplys(URLEncoder.encode("to:$name", "utf-8"), maxId)?.request(delegate(it))
        }
    }
}